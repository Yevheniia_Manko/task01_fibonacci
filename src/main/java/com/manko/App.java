package com.manko;

/**
 * The class App contains the method that is the entry point into
 * the application.
 * @author Yevheniia Manko
 * @version 1.0
 */
public class App {

    /**
     * The entry point into the application.
     * @param args - command-line arguments
     */
    public static void main(String[] args) {
        UserMenu ui = new UserMenu();
        ui.start();
    }
}
