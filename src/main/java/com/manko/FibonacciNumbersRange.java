package com.manko;

import java.util.ArrayList;
import java.util.List;

/**
 * The class FibonacciNumbersRange contains methods for work with range of
 * Fibonacci numbers that are in the defined interval.
 * @author Yevheniia Manko
 * @version 1.0
 */
public class FibonacciNumbersRange {

    /**
     * A List that stores values of all Fibonacci numbers
     * in the defined range.
     * */
    private List<Integer> allNumbers = new ArrayList<>();

    /**
     * A List that stores values of odd Fibonacci numbers
     * in the defined range.
     * */
    private List<Integer> oddNumbers = new ArrayList<>();

    /**
     * A List that stores values of even Fibonacci numbers
     * in the defined range.
     * */
    private List<Integer> evenNumbers = new ArrayList<>();


    /**
     * Constructs a new FibonacciNumbersRange object that represents the range
     * of Fibonacci numbers in the interval between start value and end value.
     * @param start - the start value of the range
     * @param end - the end value of the range
     */
    public FibonacciNumbersRange(int start, int end) {
        setRangeFibonacciNumbers(start, end);
        setOddNumbers();
        setEvenNumbers();
    }

    /**
     * Initializes a list that stores values of all Fibonacci numbers
     * in the interval between start value and end value.
     * @param start - the start value of the range
     * @param end - the end value of the range
     */
    private void setRangeFibonacciNumbers(int start, int end) {
        List<Integer> listFibonacciNumbers = getFibonacciNumbers(end);
        for (Integer num : listFibonacciNumbers) {
            if (num >= start) {
                allNumbers.add(num);
            }
        }
    }

    /**
     * Creates a list that stores values of all Fibonacci numbers
     * in the interval between 0 and the end value.
     * @param end - the end value of the range
     * @return a list that stores values of all Fibonacci numbers
     * in the interval between 0 and the end value
     */
    private List<Integer> getFibonacciNumbers(int end) {
        List<Integer> listFibonacciNumbers = new ArrayList<>();
        listFibonacciNumbers.add(0);
        int nextNumber = 1;
        int i = 1;
        while (nextNumber <= end) {
            listFibonacciNumbers.add(nextNumber);
            i++;
            nextNumber = listFibonacciNumbers.get(i - 1)
                    + listFibonacciNumbers.get(i - 2);
        }
        return listFibonacciNumbers;
    }

    /**
     * Creates a list that stores all odd Fibonacci numbers
     * in the defined interval.
     */
    private void setOddNumbers() {
        for (Integer number : allNumbers) {
            if (number % 2 != 0) {
                oddNumbers.add(number);
            }
        }
    }

    /**
     * Creates a list that stores all even Fibonacci numbers
     * in the defined interval.
     */
    private void setEvenNumbers() {
        for (Integer number : allNumbers) {
            if (number % 2 == 0) {
                evenNumbers.add(number);
            }
        }
    }

    /**
     * Prints all Fibonacci numbers in the defined interval.
     */
    public void printAll() {
        for (Integer number : allNumbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Prints odd Fibonacci numbers in the defined interval.
     */
    public void printOddNumbers() {
        for (Integer number : oddNumbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Prints even Fibonacci numbers in the defined interval.
     */
    public void printEvenNumbers() {
        for (Integer number : evenNumbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Prints percentage of even and odd Fibonacci numbers
     * in the defined interval.
     */
    public void printPercentage() {
        int countOddNumbers = oddNumbers.size();
        int countEvenNumbers = evenNumbers.size();
        int totalNumbers = countOddNumbers + countEvenNumbers;
        int totalPercentage = 100;
        int percentageOddNumbers = countOddNumbers * totalPercentage / totalNumbers;
        int percentageEvenNumbers = totalPercentage - percentageOddNumbers;
        System.out.println("- odd Fibonacci numbers = " + percentageOddNumbers + "%");
        System.out.println("- even Fibonacci numbers = " + percentageEvenNumbers + "%");
    }
}
