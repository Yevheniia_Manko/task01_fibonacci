package com.manko;

import java.util.Scanner;

/**
 * The class UserMenu contains methods for interaction
 * with the user in the console.
 * @author Yevheniia Manko
 * @version 1.0
 */
public class UserMenu {

    /**
     * An int value that is the start value of the range.
     */
    private int startValue;

    /**
     * An int value that is the end value of the range.
     */
    private int endValue;

    /**
     * A Scanner object for parsing input data from the user.
     */
    private Scanner sc = new Scanner(System.in);

    /**
     * A FibonacciNumbersRange object that represents Fibonacci numbers
     * in the user-defined interval.
     */
    private FibonacciNumbersRange range;


    /**
     * Initializes instance variable range.
     */
    public UserMenu() {
        readStartValue();
        readEndValue();
        range = new FibonacciNumbersRange(startValue, endValue);
    }

    /**
     * Takes the user's input, parses it and return an int value.
     * @return - the int value
     */
    private int readIntegerValue() {
        int value;
        while (true) {
            try {
                value = Integer.parseInt(sc.nextLine());
                break;
            } catch (Exception e) {
                System.out.println("You entered invalid value.");
                System.out.println("Please try again.");
            }
        }
        return value;
    }

    /**
     * Initializes the instance variable startValue.
     */
    private void readStartValue() {
        System.out.println("Please enter the start value of the range:");
        startValue = readIntegerValue();
    }

    /**
     * Initializes the instance variable endValue.
     */
    private void readEndValue() {
        System.out.println("Please enter the end value of the range:");
        endValue = readIntegerValue();
    }

    /**
     * Outputs the menu items on the console.
     * @return the int value that represents the user's choice
     */
    private int menu() {
        System.out.println("---Choose the menu item---");
        System.out.println("1. Print Fibonacci numbers in the defined interval.");
        System.out.println("2. Print odd Fibonacci numbers in the defined interval.");
        System.out.println("3. Print even Fibonacci numbers in the defined interval.");
        System.out.println("4. Print percentage of odd and even Fibonacci numbers in the defined interval.");
        System.out.println("0. Exit");
        return readMenuItem(0, 4);
    }

    /**
     * Takes user's input, checks it and returns it.
     * @param min - the minimum value of the menu item
     * @param max - the maximum value of the menu item
     * @return the int value that represents the user's choice
     */
    private int readMenuItem(int min, int max) {
        int choice;
        while (true) {
            choice = readIntegerValue();
            if (choice >= min && choice <= max) {
                break;
            } else {
                System.out.println("You entered invalid value.");
                System.out.println("Please enter the menu item.");
            }
        }
        return choice;
    }

    /**
     * Controls the execution of the program depending on the user's choice.
     */
    public void start() {
        boolean oneMore = true;
        while (oneMore) {
            int choice = menu();
            switch (choice) {
                case 0:
                    System.out.println("The end.");
                    oneMore = false;
                    break;
                case 1:
                    printAllFibonacciNumbers();
                    break;
                case 2:
                    printOddFibonacciNumbers();
                    break;
                case 3:
                    printEvenFibonacciNumbers();
                    break;
                case 4:
                    printPercentageOddAndEvenFibonacciNumbers();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Prints all Fibonacci numbers in the user-defined interval.
     */
    private void printAllFibonacciNumbers() {
        System.out.println("Numbers Fibonacci in the interval between " + startValue + " and " + endValue + " are:");
        range.printAll();
    }

    /**
     * Prints all odd Fibonacci numbers in the user-defined interval.
     */
    private void printOddFibonacciNumbers() {
        System.out.println("Odd numbers Fibonacci in the interval between " + startValue
                + " and " + endValue + " are:");
        range.printOddNumbers();
    }

    /**
     * Prints all even Fibonacci numbers in the user-defined interval.
     */
    private void printEvenFibonacciNumbers() {
        System.out.println("Even numbers Fibonacci in the interval between " + startValue
                + " and " + endValue + " are:");
        range.printEvenNumbers();
    }

    /**
     * Prints percentage of odd and even Fibonacci numbers in the user-defined interval.
     */
    private void printPercentageOddAndEvenFibonacciNumbers() {
        System.out.print("Percentage of odd and even Fibonacci numbers ");
        System.out.println("in the interval between " + startValue
                + " and " + endValue + " is:");
        range.printPercentage();
    }
}
